#include "Grille.h"
#include "Case.h"


Grille::Grille()
{

}

Grille::Grille(const int tailleX, const int tailleY, std::vector<sf::Vector2i> casesAllumables)
    : m_tailleX{tailleX}, m_tailleY{tailleY}, m_casesAllumables{casesAllumables}
{
    std::vector< std::vector<Case> > niveau(m_tailleX, std::vector<Case>(m_tailleY));
    m_niveau = niveau;
    for(int i = 0; i < m_tailleX ; i++)
    {
        for(int j = 0; j < m_tailleY; j++)
        {
           Case lambda;
           if(j%2==0)
           {
                lambda.m_hexagone.setPosition((float)(i*54) + 40. ,(float)(j*46) + 50.);
           }
           else
           {
               lambda.m_hexagone.setPosition((float)(i*54) + 67. , (float)(j*46 + 50.));
           }
           m_niveau[i][j] = lambda;
        }
    }
    for(sf::Vector2i elem : m_casesAllumables)
    {
        m_niveau[elem.x][elem.y].RendreAllumable();
    }
}

void Grille::AfficherGrille(sf::RenderWindow* fenetre)
{
    for(int i = 0; i < m_tailleX; i++)
    {
        for(int j = 0; j < m_tailleY; j++)
        {
            if(m_niveau[i][j].siPassable())
            {
                fenetre->draw(m_niveau[i][j].m_hexagone);
            }
            if(!m_niveau[i][j].siPassable())
            {
                m_niveau[i][j].m_hexagone.setFillColor(sf::Color::White);
                fenetre->draw(m_niveau[i][j].m_hexagone);
            }
            if(m_niveau[i][j].siAllumable())
            {
                m_niveau[i][j].m_hexagone.setFillColor(sf::Color::Blue);
                m_casesAllumables.push_back({i, j});
                fenetre->draw(m_niveau[i][j].m_hexagone);
            }
        }
    }
}

void Grille::RendreAllumable(const int i, const int j)
{
    m_niveau[i][j].RendreAllumable();
    m_casesAllumables.push_back({ i, j });

}

std::vector<std::vector<Case>> Grille::accesGrille()
{
    return m_niveau;
}

bool Grille::testBornesGrillesX(int aTester)
{
    return aTester < m_tailleX;
}

bool Grille::testBornesGrillesY(int aTester)
{
    return aTester < m_tailleY;
}

bool Grille::gagne()
{
    bool gagne = true;
    for(std::vector<Case> colonne : m_niveau)
    {
        for(Case elem : colonne)
        {
            if(elem.siAllumable())
            {
                gagne = false;
            }
        }
    }
    return gagne;
}

void Grille::Allumer(const int i, const int j, std::vector<sf::Vector2i>  *casesAllumablesDemo, bool & enDemo)
{
    m_niveau[i][j].Allumer();
    if(enDemo)
    {
        sf::Vector2i tmp = {i,j};
        std::vector<sf::Vector2i>::iterator it = find(casesAllumablesDemo->begin(), casesAllumablesDemo->end(), tmp);
        casesAllumablesDemo->erase(it);
    }

}

void Grille::nouvelleTaille(const int x, const int y)
{
    m_tailleX = x;
    m_tailleY = y;
}

void Grille::remiseAZero()
{
    for(sf::Vector2i elem : m_casesAllumables)
    {
        m_niveau[elem.x][elem.y].RendreAllumable();
    }
}

sf::Vector2i Grille::tailleDeGrille()
{
    return {m_tailleX, m_tailleY};
}
