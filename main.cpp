#include <iostream>
#include "Appli.cpp"

using namespace std;
const float LARGEUR = 1000.;
const float LONGUEUR = 600.;
const string TITRE = "Hex LightBot";



int main()
{
   sf::ContextSettings parametres;
   parametres.antialiasingLevel = 8;
   Appli app{ LARGEUR, LONGUEUR, TITRE, parametres };
   app.marche();
}
