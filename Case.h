#ifndef CASE_H
#define CASE_H

#include <SFML/Graphics.hpp>
#include <iostream>

class Case //Une case est un support sur lequel peut se déplacer le robot
{
public:
    Case();
    sf::CircleShape m_hexagone;
    enum class EtatCase{PASSABLE, NON_PASSABLE, ALLUMABLE} m_etatCase;
    void Allumer();
    void RendreAllumable();
    bool siAllumable();
    bool siPassable();
};

#endif // CASE_H
