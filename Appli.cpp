#include <unistd.h>
#include <dirent.h>
#include <fstream>

#include "Appli.h"
#include "Sequence.h"
#include "Action.h"
#include "Grille.h"
#include "Robot.h"
#include "Controle.h"

using Position = sf::Vector2f;

const Position LOGO = { 350., 75. };
const Position JEU = { 300. , 375. };
const Position EDIT = { 570. , 375. };
const Position TAILLE_ICONES = {25., 25.};  // Ce sont les positions des icones du menu.
const float RAYON = 50.; // Le rayon des cases.

int cmptClicP1 = 0;
int cmptClicP2 = 0; // Ce sont les compteurs de clics, on les utilise pour savoir quelle sequence on compose (P1 ou P2).

Sequence defaut(280, 50, 50., 500., {1,2,3,4,5,6}, true, false);
Sequence sequencePrincipale(180, 150, 800., 50., {}, false, true);
Sequence sequenceP1(180, 100, 800., 225., {}, false, false);
Sequence sequenceP2(180, 100, 800., 350., {}, false, false); //Ce sont toutes les sequences qui apparaissent dans le jeu.

sf::Vector2i pos; //C'est le vecteur d'entiers qui détermine la taille d'une grille editée.

bool dansMenu = true;
bool enEdition = false;
bool enLecture = false;
bool tailleXTrouvee = false;
bool tailleYTrouvee = false;
bool enDemonstration = false; //Ce sont des booléens dont l'etat indique où l'on se situe dans les menus

Appli::Appli(const float largeur, const float longueur, const std::string titre, const sf::ContextSettings &param)
    :m_fenetre{ {largeur, longueur}, titre, sf::Style::Default, param }//On construit une fenêtre.
{
    m_arret = false; //Si la fenetre se ferme.
    m_modeP1 = false;//Ce booléen est vrai si on est en train de composer P1
    m_modeP2 = false;//Ce booléen est vrai si on est en train de composer P2
    m_etat = Etat::INITIAL;
}

void Appli::creationSequence(Sequence &defaut, Sequence & sequenceP1)
{
    int limite = 12;
    if(!sequenceP1.estPrincipale) //on impose une limite de 12 actions par sequence, 8 si elle n'est pas principale
        limite = 8;
    for(Action elem : defaut.AccesTab())
    {
       if(souris_dans_rectangle(elem.AccesseurIcone().getPosition(), TAILLE_ICONES ) && sequenceP1.AccesTab().size() < limite)
       {
           sequenceP1.AjoutAction(elem.accesNum());
           m_souris = {0,0};
       }
       //si on clique sur une action de la séquence par défaut on l'ajoute à celle qu'on est en train de composer
    }

}

/*void Appli::supprimeAction(Sequence &lue)
{
    for(Action elem2 : lue.AccesTab())
    {
       if(souris_dans_rectangle(elem2.AccesseurIcone().getPosition(), TAILLE_ICONES ) && lue.AccesTab().size() > 0)
       {
           lue.SupprimeAction(elem2.accesNum());
           m_souris = {0,0};
       }
    }
}*/

void Appli::lectureSequence(Sequence & lue, Sequence &P1, Sequence &P2, Robot & joueur, Grille & niveau, std::vector<sf::Vector2i> *casesAllumablesDemo)
{

    for(Action elem : lue.AccesTab())
    {
        switch (elem.accesNum()) {
        case 1: // ici le robot change de case selon sa direction.
            joueur.deplacementRobotsurGrille(&m_fenetre, niveau);
            m_souris = {0,0};
            break;
        case 2:// ici il change de direction pour prendre celle qui est à sa gauche.
            joueur.TournerAGauche(&m_fenetre, niveau);
            m_souris = {0,0};
            break;
        case 3:// ici il change de direction pour prendre celle qui est à sa droite.
            joueur.TournerADroite(&m_fenetre, niveau);
            m_souris = {0,0};
            break;
        case 4:// ici il allume la case sur laquelle il se trouve.
            joueur.AllumerCase(niveau, &m_fenetre, casesAllumablesDemo, enDemonstration);
            m_souris = {0,0};
        case 5:// on lit la séquence P1. Les boucles ne fonctionnent pas.
            lectureSequence(P1,P2,P1,joueur,niveau,casesAllumablesDemo);
            break;
        case 6://on lit la séquence P2.
            lectureSequence(P2,P1,P2,joueur,niveau,casesAllumablesDemo);
            break;
        default:
            break;
        }
    }


}

void Appli::remiseAZero(Grille & niveau ,Robot &joueur, Sequence &Principale, Sequence &P1, Sequence &P2)
{

    joueur.remiseAZero(); //Le robot se met sur la case le plus en haut à gauche.
    Principale.viderActions();
    P1.viderActions();
    P2.viderActions(); // on libère toutes les séquences de leurs actions.
}

void Appli::menu() //l'écran de présentation
{

    sf::Texture image;
    image.loadFromFile("titre2.png"); //Le logo titre
    sf::Sprite sp(image);
    sp.setPosition(LOGO);

    sf::CircleShape jeu(RAYON, 6); //Le bouton pour accéder au mode de jeu
    jeu.setFillColor(sf::Color::Blue);
    jeu.setPosition(JEU);

    sf::CircleShape play(20, 3); //Un logo play qui se met sur le bouton de jeu
    play.setPosition({jeu.getPosition().x + 70., jeu.getPosition().y + 30.});
    play.setFillColor(sf::Color::White);
    play.rotate(90.);

    sf::CircleShape edition(RAYON, 6); //le bouton pour accéder au mode d'édition
    edition.setFillColor(sf::Color::Black);
    edition.setPosition(EDIT);

    sf::Font fonte;
    sf::Text texte;//Un texte indicatif sur le menu d'édition.
    if(fonte.loadFromFile("imagine_fonte.ttf"))
    {
        texte.setFont(fonte);
        texte.setString("edition");
        texte.setCharacterSize(15);
        texte.setColor(sf::Color::White);
        texte.setPosition({edition.getPosition().x + 20., edition.getPosition().y + 40.});
    }

    m_fenetre.draw(sp);
    m_fenetre.draw(jeu);
    m_fenetre.draw(edition);
    m_fenetre.draw(play);
    m_fenetre.draw(texte);//On dessine tout sur la fenêtre.
    m_fenetre.display();//On rafraîchit l'affichage.
}

void Appli::choixMenu() //si l'on clique sur le bouton de jeu, l'application se met en
//état de jeu, et en état d'edition si l'on clique sur le bouton d'édition.
//Cela fonctionne que si l'on est dans le menu d'accueil (d'où dansMenu).
{
    if(souris_dans_rond(JEU) && dansMenu)
    {
       m_etat = Etat::JOUER;
    }
    else if(souris_dans_rond(EDIT) && dansMenu)
    {
       m_etat = Etat::EDITER;
    }
}


void Appli::jeu(Robot & joueur, Grille & niveau, std::vector<sf::Vector2i>* casesAllumablesDemo)
{

        Controle play(350., 500. ,"Jeu");//le controle qui sert à lancer la séquence principale.
        Controle arret(425.,500., "Stop");//le controle qui sert à effacer le contenu de toutes les séquences.
        Controle P1(500., 500., "ModeP1");//le controle qui sert à composer la séquence P1.
        Controle P2(575., 500., "ModeP2");//le controle qui sert à composer la séquence P2.

        niveau.AfficherGrille(&m_fenetre);
        joueur.dessinerRobot(&m_fenetre, niveau);
        sequencePrincipale.AppliqueSurFenetre(&m_fenetre); //on affiche le cadre de la séquence principale.
        defaut.AppliqueSurFenetre(&m_fenetre);

        if(!m_modeP1 && !m_modeP2)
            creationSequence(defaut, sequencePrincipale);
        if(m_modeP1 && !m_modeP2)
        {
            P1.appui();
            creationSequence(defaut, sequenceP1);
        }
        if(!m_modeP1 && m_modeP2)
        {
            P2.appui();
            creationSequence(defaut, sequenceP2);
        }
        //On choisit la séquence à créer selon le contrôle sur lequel on a appuyé.

        sequenceP1.AppliqueSurFenetre(&m_fenetre);
        sequenceP2.AppliqueSurFenetre(&m_fenetre);

        play.dessinControle(&m_fenetre);
        arret.dessinControle(&m_fenetre);
        P1.dessinControle(&m_fenetre);
        P2.dessinControle(&m_fenetre);

        if(souris_dans_rectangle(play.bouton().getPosition(), play.bouton().getSize()))
        {
            play.appui(); //on met le contrôle play en bleu
            play.dessinControle(&m_fenetre);
            lectureSequence(sequencePrincipale,sequenceP1,sequenceP2, joueur, niveau, casesAllumablesDemo);
            m_souris = {0,0};
            if(!niveau.gagne())
            {
                niveau.remiseAZero();
                joueur.remiseAZero();
            }
        }
        //Si on appuie sur le contrôle play on éxecute la séquence composée, si le niveau n'est pas gagné après lecture
        //tout se remet à zéro sauf les séquences composées par le joueur.

        if(souris_dans_rectangle(arret.bouton().getPosition(), arret.bouton().getSize()))
        {
            arret.appui(); //on met le contrôle stop en bleu
            arret.dessinControle(&m_fenetre);
            remiseAZero(niveau,joueur, sequencePrincipale, sequenceP1, sequenceP2);
            m_souris = {0,0};
        }
        //Si on clique sur le contrôle stop on efface le contenu de toutes les séquences.


        if(souris_dans_rectangle(P1.bouton().getPosition(), P1.bouton().getSize()))
        {
            if(cmptClicP1 % 2 == 0 || m_modeP2 || !m_modeP1)
            {
                P1.appui();
                P1.dessinControle(&m_fenetre);
                m_modeP1 = true;
                m_modeP2 = false;
                cmptClicP1++;
                m_souris = {0,0};
            }
            else if(m_modeP1)
            {
                m_modeP1 = false;
                cmptClicP1++;
                m_souris = {0,0};
            }

        }
        //Si on clique sur le contrôle P1, on compose la séquence P1 jusqu'a ce qu'on re-clique sur le bouton ou
        //jusqu'a ce qu'on clique sur le contrôle P2.

        if(souris_dans_rectangle(P2.bouton().getPosition(), P2.bouton().getSize()))
        {

            if(cmptClicP2 % 2 == 0 || m_modeP1 || !m_modeP2)
            {
                P2.appui();
                P2.dessinControle(&m_fenetre);
                m_modeP2 = true;
                m_modeP1 = false;
                cmptClicP2++;
                m_souris = {0,0};
            }
            else if (m_modeP2)
            {
                m_modeP2 = false;
                cmptClicP2++;
                m_souris = {0,0};
            }

        }
        //Même chose que pour le contrôle P1.

        if(niveau.gagne() && !enDemonstration)
        {

                remiseAZero(niveau, joueur, sequencePrincipale, sequenceP1, sequenceP2);
                ecranVictoire();

        } //Si un niveau est gagné on remet tout à zéro car on se sert du même robot et des mêmes séquences.

        if(enDemonstration)
        {
            Controle sauvegarder(650., 500., "Sauver");
            sauvegarder.dessinControle(&m_fenetre);
            if(souris_dans_rectangle(sauvegarder.bouton().getPosition(), sauvegarder.bouton().getSize()) && niveau.gagne() )
                ecritureSauvegarde(pos, *casesAllumablesDemo);
       }
        //Puisqu'on se sert de la fonction jeu pour tester un niveau edité on utilise un booléen pour déterminer
        //si nous effectuons un test ou pas, si c'est le cas on ajoute un contrôle sauver pour sauvegarder un niveau.
}
//////////////////////////////////////////////////Fonctions provenant du tutoriel 1 sur la SFML.
float Appli::module(const Position & v)
{
    return sqrt(v.x * v.x + v.y * v.y);
}


float Appli::distance(const Position & p1, const Position & p2 )
{
    return module( p1 - p2 );
}

void Appli::set_mouse_coord(int x, int y)
//enregistre une position sur la fenêtre
{
    auto pos = m_fenetre.mapPixelToCoords({x, y});
    m_souris = { pos.x, pos.y };
}

bool Appli::souris_dans_rond(const Position & position) const
{
    return distance(m_souris, position) <= RAYON + 50.;
}

bool Appli::souris_dans_rectangle(const Position & coin, const Position & dims)
{
    auto d = m_souris - coin;
    return 0 <= d.x and d.x <= dims.x and 0 <= d.y and d.y <= dims.y;
}

bool Appli::stop()
{
    if(!m_fenetre.isOpen())
    {
        m_arret = true;
    }
    bool arrete = m_arret;
    return arrete;
}

///////////////////////////////////////////////////////////////////////////////////////
void Appli::marche()
{
    m_fenetre.setFramerateLimit(60);
    std::vector<sf::Vector2i> casesAllumablesEditees; // Un tableau stockant les cases allumables d'un niveau edité.
    Robot joueur({0,0});
    Grille niveau1(5,1, {{0,0}});
    Grille niveau2(4,4, {{3,0}, {3,2}});
    std::string saisie;//Une chaîne de caractères qui enregistre les frappes de clavier.


    while (m_fenetre.isOpen())
    {

        sf::Event event;
        while(m_fenetre.pollEvent(event))
        {
            switch(event.type)
            {
            case sf::Event::Closed :
                m_fenetre.close();
                break;
            case sf::Event::MouseButtonPressed :
                set_mouse_coord(event.mouseButton.x, event.mouseButton.y); // Le dernier clic de la souris sur l'application
                //est stocké
                choixMenu();
                break;
            case sf::Event::TextEntered:
                if (event.text.unicode > 47 && event.text.unicode < 58 && !enEdition && !dansMenu)
                    saisie += static_cast<char>(event.text.unicode);
                    //on enregistre uniquement les chiffres entrés sur le clavier lorsqu'on choisit la taille de notre grille
                    //personnalisée.
                break;
            case sf::Event::KeyPressed:
                if(event.key.code == sf::Keyboard::BackSpace && saisie.length() > 0)
                    saisie.pop_back();
                //Si on appuie sur la touche return du clavier on enlève le dernier chiffre saisi.
                break;

            }

        }
        m_fenetre.clear(sf::Color::White);

        switch(m_etat)
        {
        case Etat::INITIAL:
            menu();
            break;
        case Etat::JOUER:
        {
            dansMenu = false;//L'application ne se considère plus dans le menu d'accueil.
            lectureSauvegarde(pos, casesAllumablesEditees, enLecture);//On lit l'éventuelle sauvegarde d'un niveau edité.
            if(!enLecture)
            {
                if(!niveau1.gagne())
                     jeu(joueur, niveau1, &casesAllumablesEditees);
                Grille editee(pos.x, pos.y, casesAllumablesEditees);
                if(niveau1.gagne())
                    jeu(joueur, editee, &casesAllumablesEditees);// On joue le niveau edité en dernier.
            }


        }
            break;
        case Etat::EDITER:
            dansMenu = false;
            if(!enEdition && !enDemonstration) //Si on ne vient pas du menu d'édition.
                pos = editionTailleGrille(saisie); //On choisit la taille de notre grille qui sera affectée à pos.
            if(enEdition && !enDemonstration)
            {
                Grille editee(pos.x, pos.y, casesAllumablesEditees);
                editee.AfficherGrille(&m_fenetre);
                clic(editee, casesAllumablesEditees, joueur ,enDemonstration);//On affiche une grille sans cases
                //allumables car on les choisit en cliquant dessus.
            }
            if(enDemonstration && !enEdition)
            {
                Grille editee(pos.x, pos.y, casesAllumablesEditees);
                jeu(joueur, editee, &casesAllumablesEditees);//on teste le niveau pour voir si il est gagnable.
            }
            break;

        }

        m_fenetre.display();
    }
}



void Appli::ecranVictoire() //Un écran de félicitations entre chaque niveau.
{
    sf::Font fonte;
    sf::Text texte;
    if(fonte.loadFromFile("KeepCalm-Medium.ttf"))
    {
        texte.setFont(fonte);
        texte.setString("BRAVO ! vous avez reussi ce niveau. \n"
                        "Passons au suivant !");

        texte.setCharacterSize(30);
        texte.setColor(sf::Color::Red);
        texte.setPosition({50., 285.});
        m_fenetre.clear(sf::Color::White);

        m_fenetre.draw(texte);
        m_fenetre.display();

        sleep(2);
    }
}





sf::Vector2i Appli::editionTailleGrille(std::string & saisie)
//Cette fonction interprète les frappes de l'utilisateur pour enregistrer le choix de la taille de sa grille.
{
    int tailleX;
    int tailleY;

    sf::Font fonte;
    sf::Text texte;

    Controle valider(800.,500., "Valider"); //Un contrôle pour valider le chiffre entré.
    Controle finir(900.,500., "Fin_Edit_Taille");//Un contrôle pour finir l'édition de la taille

    fonte.loadFromFile("KeepCalm-Medium.ttf");
    texte.setFont(fonte);
    texte.setString(" Bonjour et bienvenue dans le menu d'edition. \n"
                    "   Entrez un chiffre entre 1 et 12 afin de \n"
                    " choisir la taille horizontale de votre grille \n");
    texte.setPosition({120.,150.});
    texte.setCharacterSize(30);
    texte.setColor(sf::Color::Black);

    sf::Text entree;
    entree.setFont(fonte);
    entree.setCharacterSize(30);
    entree.setColor(sf::Color::Black);
    entree.setString(saisie);
    entree.setPosition({490., 550.}); //


    if(souris_dans_rectangle(valider.bouton().getPosition(), valider.bouton().getSize()))
    {
        tailleX = std::atoi(saisie.c_str());
        if(tailleX >= 1 && tailleX <= 12)
        {
           valider.appui();
           tailleXTrouvee = true;
           m_souris = {0,0};//Si on appuie sur valider et que la valeur respecte les limites de taille
           // on passe à l'écran suivant qui permet de choisir la taille verticale.
        }
        else
        {
            m_souris = {0,0};
        }
    }

    if(tailleXTrouvee)//Si on a choisi notre taille horizontale.
    {
        texte.setString("Entrez maintenant un chiffre entre 1 et 7 \n"
                        "   pour definir la taille verticale.\n");
        finir.dessinControle(&m_fenetre);
        tailleY = std::atoi(saisie.c_str());
        if (souris_dans_rectangle(finir.bouton().getPosition(), finir.bouton().getSize()))
        {
            if(tailleY >= 1 && tailleY <= 7)
            {
                finir.appui();
                tailleYTrouvee = true;
                //Si on appuie sur finir et que la valeur respecte les limites de taille
                // on passe au choix des cases allumables.

            }
        }


    }
    m_fenetre.draw(texte);
    m_fenetre.draw(entree);
    if(!tailleXTrouvee)
        valider.dessinControle(&m_fenetre);

    if(tailleYTrouvee)
    {
        sf::Vector2i editee = {tailleX, tailleY};
        m_fenetre.clear(sf::Color::White);
        enEdition = true;
        return editee;

    }


}
void Appli::clic(Grille & editee, std::vector<sf::Vector2i> & casesAllumablesEditees,Robot & joueur, bool & enDemonstration)
//Permet à l'utilisateur de choisir les cases qu'il souhaite rendre allumable.
{
   sf::Font fonte;
   sf::Text choixCasesAllumables;

   Controle stop(800., 500., "Stop");
   Controle valider(875., 500., "Valider"); // Contrôle pour enregistrer le choix des cases.


   if (fonte.loadFromFile("KeepCalm-Medium.ttf"))
   {
        choixCasesAllumables.setString("Cliquez sur les cases que vous souhaitez rendre allumable.");
        choixCasesAllumables.setFont(fonte);
        choixCasesAllumables.setCharacterSize(20);
        choixCasesAllumables.setPosition({50., 500.});
        choixCasesAllumables.setColor(sf::Color::Black);
        m_fenetre.draw(choixCasesAllumables);
   }

   int caseCliqueeX = (m_souris.x - 40.) / 54;
   int caseCliqueeY = (m_souris.y - 50.) / 46.; //on convertit la position de la souris en pixels en position en cases.

   if(caseCliqueeY % 2 == 1)//Puisque la grille ne forme pas un carré il faut distinguer les rangées paires et impaires.
       caseCliqueeX = (m_souris.x - 67.) / 54;

   if(caseCliqueeX < pos.x && caseCliqueeY < pos.y)//si la case est bien dans les limites de taille
   {
       if(!editee.accesGrille()[caseCliqueeX][caseCliqueeY].siAllumable())//si elle n'est pas allumable.
       {
            editee.RendreAllumable(caseCliqueeX,caseCliqueeY);
            casesAllumablesEditees.push_back({caseCliqueeX, caseCliqueeY});
            m_souris = {1000., 600.};//On la rend allumable.

       }
       else if (editee.accesGrille()[caseCliqueeX][caseCliqueeY].siAllumable())//si elle est déja allumable
       {
            sf::Vector2i tmp = {caseCliqueeX, caseCliqueeY};
            std::vector<sf::Vector2i>::iterator it2 = find(casesAllumablesEditees.begin(), casesAllumablesEditees.end(), tmp);
            casesAllumablesEditees.erase(it2);// On la rend non-allumable.
            m_souris = {1000., 600.};
       }

   }
   editee.AfficherGrille(&m_fenetre);

   stop.dessinControle(&m_fenetre);
   valider.dessinControle(&m_fenetre);

   if(souris_dans_rectangle(stop.bouton().getPosition(), stop.bouton().getSize()))
       casesAllumablesEditees.clear();
// Si on clique dans stop on rend toutes les cases non-allumables.

   if(souris_dans_rectangle(valider.bouton().getPosition(), valider.bouton().getSize()) && casesAllumablesEditees.size() != 0)
   {
       m_fenetre.clear(sf::Color::White);
       enDemonstration = true;
       enEdition = false;
   }
   //Si on clique sur valider avec au moins une case allumable, on teste le niveau.
}



void Appli::ecritureSauvegarde(sf::Vector2i & pos , std::vector<sf::Vector2i> & casesAllumablesDemo)
//Cette fonction permet d'écrire un fichier de sauvegarde avec la taille de la grille en première ligne
//puis les positions de toutes les cases allumables.
{
    std::ofstream sauvegarde("niveau.txt", std::ios::out | std::ios::trunc);

    if(sauvegarde)
    {
        sauvegarde << "Taille: " << pos.x << "," << pos.y << std::endl;
        for(sf::Vector2i elem : casesAllumablesDemo)
        {
            sauvegarde << elem.x << "," << elem.y << std::endl;
        }
        sauvegarde.close();
    }
    else
    {
        std::cerr << "Erreur a l'ouverture du fichier";
    }
}

void Appli::lectureSauvegarde(sf::Vector2i & pos, std::vector<sf::Vector2i> & casesAllumablesDemo, bool & enLecture)
// Cette fonction permet d'affecter deux variables avec des données lues sur le fichier, la grille personnalisée
// sera reconstruite avec ces deux variables.
{
    enLecture = true;
    std::ifstream lecture("niveau.txt", std::ios::in);
    std::string tmp;
    int i = 0;
    int j = 7;
    int k = 0;
    if(lecture)
    {
        while(getline(lecture, tmp))
        {
            if(tmp[0] == 'T')
            {
                while(tmp[j] != ',')
                {
                    i++;
                    j++;
                }
                std::string x = tmp.substr(7, 7);
                std::string y = tmp.substr(7+i+1, tmp.size()-1);
                int tailleX = atoi(x.c_str());
                int tailleY = atoi(y.c_str());
                pos = {tailleX, tailleY};
                i = 0;
            }
            else
            {
                while(tmp[k] != ',')
                {
                    k++;
                }
                k = 0;
                std::string caseX = tmp.substr(0, k+1);
                std::string caseY = tmp.substr(k+2, tmp.size()-1);
                int coordX = atoi(caseX.c_str());
                int coordY = atoi(caseY.c_str());
                sf::Vector2i tmp2 = {coordX, coordY};
                casesAllumablesDemo.push_back(tmp2);
            }
        }
        lecture.close();
    }
    else
    {
        std::cerr << "Erreur: Fichier non trouvé.";
        pos = {1,1};
        casesAllumablesDemo  = {{0,0}};// au cas où aucun fichier ne serait dans le dossier
        //on crée un niveau d'une case allumable.
    }
    enLecture = false;

}
