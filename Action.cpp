#include "Action.h"
#include "Sequence.h"

Action::Action(const int num)
    : m_numero(num)
{}

sf::Sprite Action::AccesseurIcone() // Accesseur qui retourne la variable m_sprite
{
    return m_sprite;
}

void Action::attribution(Sequence & seq, const int indice, const int y)
{
    switch(m_numero)  // ici on attribue un nom(qui sert à déterminer le fichier image à utiliser)
        //en fonction de l'identifiant de l'action(m_numero)
    {
        case 1:
            m_nom = "Avancer.png";
            break;
        case 2:
            m_nom = "TourneGauche.png";
            break;
        case 3:
            m_nom = "TourneDroite.png";
            break;
        case 4:
            m_nom = "Allumer.png";
            break;
        case 5:
            m_nom = "P1.png";
            break;
        case 6:
            m_nom = "P2.png";

    }
    m_icone.loadFromFile(m_nom);
    m_sprite.setTexture(m_icone);
    m_sprite.setPosition((seq.AccesCadre().getPosition().x + 5 + indice*50),(seq.AccesCadre().getPosition().y + y));
    //on définit ici une position relative au cadre de la sequence a laquelle appartient l'action.
}

int Action::accesNum()
{
    return m_numero; //Accesseur qui retourne l'identifiant de l'action.
}

bool Action::operator ==(const Action &autre)
{
    return m_numero == autre.m_numero;
}
