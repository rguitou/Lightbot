#include "Case.h"

Case::Case()
{
    m_hexagone.setRadius(30.);
    m_hexagone.setPointCount(6);
    m_hexagone.setFillColor(sf::Color::Green);
    m_hexagone.setOutlineColor(sf::Color::Black);
    m_hexagone.setOutlineThickness(1.);

    m_etatCase = EtatCase::PASSABLE;
}

void Case::RendreAllumable()
{
    m_etatCase = EtatCase::ALLUMABLE;
}

bool Case::siAllumable()
{
    return m_etatCase == EtatCase::ALLUMABLE;
}

bool Case::siPassable()
{
    return m_etatCase == EtatCase::PASSABLE;
}

void Case::Allumer()
{
    if(siAllumable())
    {
        m_etatCase = EtatCase::PASSABLE;
        m_hexagone.setFillColor(sf::Color::Green);
    }
}
