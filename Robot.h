#ifndef ROBOT_H
#define ROBOT_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Grille.h"


class Robot // Un robot est ce que contrôle le joueur par le biais des actions qu'il lui impose.
{
private:
    sf::RectangleShape m_ligne;
    sf::Vector2i m_positionGrille;
    int m_dir;
public:
    Robot(sf::Vector2i positionGrille);
    sf::Vector2i positionRobot();
    void TournerAGauche(sf::RenderWindow* fenetre, Grille & niveau);
    void TournerADroite(sf::RenderWindow* fenetre, Grille & niveau);
    void deplacementRobotsurGrille(sf::RenderWindow *fenetre, Grille & niveau);
    void dessinerRobot(sf::RenderWindow* fenetre, Grille & niveau);
    void AllumerCase(Grille & niveau, sf::RenderWindow *fenetre, std::vector<sf::Vector2i> *casesAllumablesDemo, bool &enDemo);
    void nouvellePosition(const int x, const int y);
    void deplacementEnPixels(Grille &niveau);
    void remiseAZero();
    enum class Direction {EST, SUD_EST, SUD_OUEST, OUEST, NORD_OUEST, NORD_EST} m_direction;
    int posX();
    int posY();

};

#endif // ROBOT_H
