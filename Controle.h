#ifndef CONTROLE_H
#define CONTROLE_H

#include <SFML/Graphics.hpp>
#include <iostream>

class Controle //Un contrôle sont les biais par lesquels un joueur peut agir sur le jeu.
{
private:
    sf::RectangleShape m_base;
    enum class Fonction{JOUER_SEQ, STOPPER_SEQ, P1_SEQ, P2_SEQ, VALIDER, FINIR_TAILLE, SAUVER, FIN_DE_VIE} m_fonction;
public:
    Controle(const float pos_x, const float pos_y, const std::string usage);
    sf::RectangleShape bouton();
    void dessinControle(sf::RenderWindow* fenetre);
    void appui();
    bool estPresse;
    void rendreIndetectable();
};

#endif // CONTROLE_H
