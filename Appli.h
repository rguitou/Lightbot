#ifndef APPLI_H
#define APPLI_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Robot.h"
#include "Grille.h"

class Sequence;

using Position = sf::Vector2f;

class Appli //la classe Appli est la classe centrale du projet
{
public:
    Appli();
    Appli(const float largeur, const float longueur, const std::string titre, const sf::ContextSettings &param);
    void marche();
    void menu();
    void jeu(Robot &Joueur, Grille &test, std::vector<sf::Vector2i> *casesAllumablesDemo);
    void set_mouse_coord(int x, int y);
    void clic(Grille &editee, std::vector<sf::Vector2i> &casesAllumablesEditees, Robot &joueur, bool &enDemonstration);
    sf::Vector2i editionTailleGrille(std::string &saisie);
    void choixMenu();
    void creationSequence(Sequence &defaut , Sequence  & JoueurPrincipal);
    void remiseAZero(Grille &niveau, Robot &joueur, Sequence & Principale, Sequence & P1, Sequence & P2);
    void lectureSequence(Sequence &lue, Sequence &P1, Sequence &P2, Robot &r, Grille &niveau, std::vector<sf::Vector2i> *casesAllumablesDemo);
    void supprimeAction(Sequence &lue);
    void ecranVictoire();
    void ecritureSauvegarde(sf::Vector2i & pos, std::vector<sf::Vector2i> &casesAllumablesDemo);
    void lectureSauvegarde(sf::Vector2i & pos, std::vector<sf::Vector2i> & casesAllumablesDemo, bool &lecture);
    bool souris_dans_rond(const Position & position) const;
    bool souris_dans_rectangle(const Position & coin, const Position & dims);
    bool stop();
    static float distance(const Position & p1, const Position & p2 );
    static float module(const Position & v);
private:
    sf::RenderWindow m_fenetre;
    bool m_arret;
    enum class Etat { INITIAL, JOUER, EDITER } m_etat;
    Position m_souris;
    bool m_modeP1;
    bool m_modeP2;


};

#endif // APPLI_H
