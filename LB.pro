TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt


QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra

LIBS           += -lsfml-graphics -lsfml-window -lsfml-system

SOURCES += main.cpp \
    Appli.cpp \
    Sequence.cpp \
    Action.cpp \
    Case.cpp \
    Grille.cpp \
    Robot.cpp \
    Controle.cpp

HEADERS += \
    Appli.h \
    Sequence.h \
    Action.h \
    Case.h \
    Grille.h \
    Robot.h \
    Controle.h
