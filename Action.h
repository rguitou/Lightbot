#ifndef ACTION_H
#define ACTION_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Appli.h"

class Sequence;

class Action //la classe Action représente les mouvements pouvant être effectués par un robot
{
private:
    std::string m_nom;
    sf::Texture m_icone;
    sf::Sprite m_sprite;
    int m_numero;
public:
    Action(const int num);
    sf::Sprite AccesseurIcone();
    void attribution(Sequence &seq, const int indice, const int y);
    bool operator==(const Action &autre); // necessaire pour supprimer une action au clic
    int accesNum();

};

#endif // ACTION_H
