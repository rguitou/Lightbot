#include "Controle.h"

Controle::Controle(const float pos_x, const float pos_y, const std::string usage)
{
    m_base.setSize({50., 50.});
    m_base.setPosition(pos_x, pos_y);
    m_base.setFillColor(sf::Color::Black);
    estPresse = false;

    if(usage == "Jeu")
        m_fonction = Fonction::JOUER_SEQ;
    if(usage == "Stop")
        m_fonction = Fonction::STOPPER_SEQ;
    if(usage == "ModeP1")
        m_fonction = Fonction::P1_SEQ;
    if(usage == "ModeP2")
        m_fonction = Fonction::P2_SEQ;
    if(usage == "Valider")
        m_fonction = Fonction::VALIDER;
    if(usage == "Fin_Edit_Taille")
        m_fonction = Fonction::FINIR_TAILLE;
    if(usage == "Sauver")
        m_fonction = Fonction::SAUVER;
}

sf::RectangleShape Controle::bouton()
{
    return m_base;
}

void Controle::dessinControle(sf::RenderWindow *fenetre)
{
    if(estPresse)
        m_base.setFillColor(sf::Color::Blue);
    fenetre->draw(m_base);
    if(m_fonction == Fonction::JOUER_SEQ)
    {
        sf::CircleShape triangle(20., 3);
        triangle.setPosition(m_base.getPosition().x + 35., m_base.getPosition().y + 15.);
        triangle.setRadius(10.);
        triangle.rotate(90.);
        triangle.setFillColor(sf::Color::White);
        fenetre->draw(triangle);
    }
    if(m_fonction == Fonction::STOPPER_SEQ)
    {
        sf::RectangleShape carre;
        carre.setSize({20., 20.});
        carre.setPosition(m_base.getPosition().x + 15., m_base.getPosition().y + 15.);
        carre.setFillColor(sf::Color::White);
        fenetre->draw(carre);
    }
    if(m_fonction == Fonction::P1_SEQ)
    {
        sf::Font fonte;
        sf::Text texte;
        fonte.loadFromFile("imagine_fonte.ttf");
        texte.setFont(fonte);
        texte.setString("1");
        texte.setCharacterSize(40);
        texte.setColor(sf::Color::White);
        texte.setPosition(m_base.getPosition().x + 20., m_base.getPosition().y - 2.);
        fenetre->draw(texte);
    }

    if(m_fonction == Fonction::P2_SEQ)
    {
        sf::Font fonte;
        sf::Text texte;
        fonte.loadFromFile("imagine_fonte.ttf");
        texte.setFont(fonte);
        texte.setString("2");
        texte.setCharacterSize(40);
        texte.setColor(sf::Color::White);
        texte.setPosition(m_base.getPosition().x + 13., m_base.getPosition().y - 2.);
        fenetre->draw(texte);
    }
    if(m_fonction == Fonction::VALIDER)
    {
        sf::Font fonte;
        sf::Text texte;
        fonte.loadFromFile("imagine_fonte.ttf");
        texte.setFont(fonte);
        texte.setString("valider");
        texte.setCharacterSize(10);
        texte.setColor(sf::Color::White);
        texte.setPosition(m_base.getPosition().x + 3., m_base.getPosition().y + 20.);
        fenetre->draw(texte);

    }
    if(m_fonction == Fonction::FINIR_TAILLE)
    {
        sf::Font fonte;
        sf::Text texte;
        fonte.loadFromFile("imagine_fonte.ttf");
        texte.setFont(fonte);
        texte.setString("Finir");
        texte.setCharacterSize(10);
        texte.setColor(sf::Color::White);
        texte.setPosition(m_base.getPosition().x + 10., m_base.getPosition().y + 20.);
        fenetre->draw(texte);
    }
    if(m_fonction == Fonction::SAUVER)
    {
        sf::Font fonte;
        sf::Text texte;
        fonte.loadFromFile("imagine_fonte.ttf");
        texte.setFont(fonte);
        texte.setString("Sauver");
        texte.setCharacterSize(10);
        texte.setColor(sf::Color::White);
        texte.setPosition(m_base.getPosition().x + 5., m_base.getPosition().y + 20.);
        fenetre->draw(texte);
    }

}

void Controle::appui()
{
    if(!estPresse)
    {
        estPresse = true;
        m_base.setFillColor(sf::Color::Blue);
    }
    if(estPresse)
    {
        estPresse = false;
    }
}

void Controle::rendreIndetectable()
{
    m_base.setSize({0,0});
    m_fonction = Fonction::FIN_DE_VIE;
}
