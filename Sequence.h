#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include "Action.h"

class Sequence //Une séquence est un ensemble d'actions
{
private:
    std::vector<Action> m_seq;
    std::vector<int> m_nums;
    sf::RectangleShape m_cadre;
    bool m_estSequenceDeSelection;
public:
    Sequence(const int largeur, const int longueur , const float x, const float y, std::vector<int> num, bool selection, bool principale);
    std::vector<Action> AccesTab();
    sf::RectangleShape AccesCadre();
    void AppliqueSurFenetre(sf::RenderWindow* fenetre);
    void DansSequence(Action a);
    void AjoutAction(int nb);
    void SupprimeAction(int nb);
    void viderActions();
    int calcPosition();
    bool estPrincipale;


};

#endif // SEQUENCE_H
