#include "Sequence.h"


Sequence::Sequence(const int largeur, const int longueur , const float x, const float y, std::vector<int> nums, bool selection, bool principale)
    : m_nums{nums}, m_estSequenceDeSelection{selection}, estPrincipale{principale}
{
    m_cadre.setFillColor(sf::Color::Black);
    m_cadre.setSize({(float)(largeur), (float)(longueur)});
    m_cadre.setPosition({(float)(x),(float)(y)});
    for(int i = 0; i < m_nums.size(); i++)
    {
        Action a(m_nums[i]);
        m_seq.push_back(a);
    }
}

void Sequence::AppliqueSurFenetre(sf::RenderWindow* fenetre)
{
    fenetre->draw(m_cadre);
    int j = 0;
    int k = 0;
    for(int i = 0; i < m_nums.size(); i++)
    {
        if(j%4 == 0 && !m_estSequenceDeSelection)
        {
           j=0;
           k = k+25;
        }
        else if(m_estSequenceDeSelection)
        {
            k = 25;
        }
       m_seq[i].attribution(*this, j, k);
       fenetre->draw(m_seq[i].AccesseurIcone());
       j=j+1;

    }

}


void Sequence::DansSequence(Action a)
{
    m_seq.push_back(a);
}

void Sequence::AjoutAction(int nb)
{
   Action a(nb);
   m_nums.push_back(nb);
   m_seq.push_back(a);

}

void Sequence::SupprimeAction(int nb)
{
    std::vector<int>::iterator it1;
    it1 = find(m_nums.begin(), m_nums.end(), nb);
    m_nums.erase(it1);

    Action tmp(nb);
    std::vector<Action>::iterator it2;
    it2 = find(m_seq.begin(), m_seq.end(), tmp);
    m_seq.erase(it2);

}

std::vector<Action> Sequence::AccesTab()
{
    return m_seq;
}

sf::RectangleShape Sequence::AccesCadre()
{
    return m_cadre;
}

void Sequence::viderActions()
{
    m_seq.clear();
    m_nums.clear();
}
