#include "Robot.h"
#include <unistd.h>

const float RAYON = 30.;
const float DEMI_TRAIT = 10.;
Robot::Robot(sf::Vector2i initiale)
    :m_positionGrille{initiale}
{

   m_ligne.setSize({20, 3});
   m_ligne.setFillColor(sf::Color::Black);
   m_dir = 0;

}
void Robot::dessinerRobot(sf::RenderWindow* fenetre, Grille & niveau)
{
    deplacementEnPixels(niveau);
    if (m_dir < 0)
    {
        m_dir = 5;
    }
    switch (m_dir)
    {
    case 0:
        m_direction = Direction::EST;
        break;
    case 1:
        m_direction = Direction::SUD_EST;
        break;
    case 2:
        m_direction = Direction::SUD_OUEST;
        break;
    case 3:
        m_direction = Direction::OUEST;
        break;
    case 4:
        m_direction = Direction::NORD_OUEST;
        break;
    case 5:
        m_direction = Direction::NORD_EST;
        break;
    default:
        m_dir = 0;
        break;
    }
    switch(m_direction)
    {
        case Direction::EST:
            m_ligne.setRotation(0.);
            break;
        case Direction::SUD_EST:
            m_ligne.setRotation(60.);
            m_ligne.move({10.,0.});
            break;
        case Direction::SUD_OUEST:
            m_ligne.setRotation(120.);
            m_ligne.move({10.,0.});
            break;
        case Direction::OUEST:
            m_ligne.setRotation(180.);
            m_ligne.move({8.,0.});
            break;
        case Direction::NORD_OUEST:
            m_ligne.setRotation(240.);
            m_ligne.move({8.,3.});
            break;
        case Direction::NORD_EST:
            m_ligne.setRotation(300.);
            m_ligne.move({5.,0.});
            break;

    }

    fenetre->draw(m_ligne);



}

sf::Vector2i Robot::positionRobot()
{
    return m_positionGrille;
}

void Robot::TournerAGauche(sf::RenderWindow* fenetre, Grille & niveau)
{
    m_dir--;
    dessinerRobot(fenetre, niveau);
}

void Robot::TournerADroite(sf::RenderWindow* fenetre, Grille & niveau)
{
    m_dir++;
    dessinerRobot(fenetre, niveau);
}

void Robot::deplacementRobotsurGrille(sf::RenderWindow* fenetre, Grille & niveau)
{

  switch(m_direction)
  {
      case Direction::EST:
       if (niveau.testBornesGrillesX(m_positionGrille.x))
            m_positionGrille.x++;
          break;
      case Direction::SUD_EST:
      if (niveau.testBornesGrillesY(m_positionGrille.y))
      {
          if(m_positionGrille.y % 2 == 0)
          {
            m_positionGrille.y++;
          }
          else
          {
             m_positionGrille = {m_positionGrille.x + 1, m_positionGrille.y + 1};
          }

      }

          break;
      case Direction::SUD_OUEST:
      if (m_positionGrille.x > 0 && niveau.testBornesGrillesY(m_positionGrille.y))
      {
          if(m_positionGrille.y % 2 == 0)
          {
            m_positionGrille = {m_positionGrille.x - 1, m_positionGrille.y + 1};
          }
          else
          {
            m_positionGrille.y++;
          }
      }
          break;
      case Direction::OUEST:
      if (m_positionGrille.x > 0)
          m_positionGrille.x--;
          break;
      case Direction::NORD_OUEST:
      if (m_positionGrille.x > 0 && m_positionGrille.y > 0)
      {
          if(m_positionGrille.y % 2 == 0)
          {
            m_positionGrille = {m_positionGrille.x - 1, m_positionGrille.y - 1};
          }
          else
          {
             m_positionGrille.y--;
          }

      }
      else if(m_positionGrille.y > 0)
      {
          if(m_positionGrille.y % 2 == 1)
          {
            m_positionGrille.y--;
          }
      }


          break;
      case Direction::NORD_EST:
      if (m_positionGrille.y > 0)
      {
          if(m_positionGrille.y % 2 == 0)
          {
             m_positionGrille = {m_positionGrille.x, m_positionGrille.y - 1};
          }
          else
          {
             m_positionGrille = {m_positionGrille.x + 1, m_positionGrille.y - 1};
          }
      }
          break;


  }

  deplacementEnPixels(niveau);
  niveau.AfficherGrille(fenetre);
  fenetre->draw(m_ligne);
  fenetre->display();
  sleep(1);

}

int Robot::posX()
{
    return m_positionGrille.x;
}

int Robot::posY()
{
    return m_positionGrille.y;
}

void Robot::AllumerCase(Grille &niveau, sf::RenderWindow *fenetre, std::vector<sf::Vector2i>* casesAllumablesDemo, bool & enDemo)
{
   niveau.Allumer(m_positionGrille.x, m_positionGrille.y, casesAllumablesDemo, enDemo);
   fenetre->draw(niveau.accesGrille()[m_positionGrille.x][m_positionGrille.y].m_hexagone);
   fenetre->draw(m_ligne);
   fenetre->display();
   sleep(1);
}

void Robot::nouvellePosition(const int x, const int y)
{
    m_positionGrille = {x,y};
}

void Robot::deplacementEnPixels(Grille &niveau)
{
    m_ligne.setPosition(niveau.accesGrille()[m_positionGrille.x][m_positionGrille.y].m_hexagone.getPosition().x + RAYON + 5 - DEMI_TRAIT, niveau.accesGrille()[m_positionGrille.x][m_positionGrille.y].m_hexagone.getPosition().y+ RAYON);
}

void Robot::remiseAZero()
{
    nouvellePosition(0,0);
    m_dir = 0;
}
