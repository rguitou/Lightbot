#ifndef GRILLE_H
#define GRILLE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include "Case.h"

class Grille //Une Grille est un ensemble de cases constituant un niveau
{
    std::vector<std::vector<Case>> m_niveau;
    std::vector<sf::Vector2i> m_casesAllumables;
    int m_tailleX;
    int m_tailleY;
public:
    Grille();
    Grille(const int tailleX, const int tailleY, std::vector<sf::Vector2i> casesAllumables);
    std::vector<std::vector<Case>> accesGrille();
    sf::Vector2i tailleDeGrille();
    void AfficherGrille(sf::RenderWindow* fenetre);
    void RendreAllumable(const int i, const int j);
    void Allumer(const int i, const int j, std::vector<sf::Vector2i> *casesAllumablesDemo, bool & enDemo);
    void nouvelleTaille(const int x, const int y);
    void remiseAZero();
    bool testBornesGrillesX(int aTester);
    bool testBornesGrillesY(int aTester);
    bool gagne();
};

#endif // GRILLE_H
